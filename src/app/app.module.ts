import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';

import { GoogleAnalyticsModule, GA_TOKEN } from 'angular-ga';
import { FacebookTrackingEventService } from './services/facebook-tracking-event-service';


const appRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'home',
        loadChildren: 'app/modules/home/home.module#HomeModule',
      },
      {
        path: 'not-found',
        loadChildren: 'app/modules/not-found/not-found.module#NotFoundModule',
      },
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: '**', redirectTo: 'not-found' }
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(
      appRoutes,
      {
        useHash: true,
      }
    ),
    CoreModule,
    GoogleAnalyticsModule.forRoot(),
    HttpClientModule,
  ],
  exports: [RouterModule],
  providers: [{ provide: GA_TOKEN, useValue: 'UA-118601095-1' }, FacebookTrackingEventService],
  bootstrap: [AppComponent]
})
export class AppModule { }
