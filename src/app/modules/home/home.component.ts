import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogComponent } from '../../core/dialog/dialog.component';
import { FacebookTrackingEventService } from '../../services/facebook-tracking-event-service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [FacebookTrackingEventService]
})
export class HomeComponent implements OnInit {

  public teamMembers: any[];
  public advisors: any[];
  public logos: any[];
  public infos: any[];

  constructor(
    public dialog: MatDialog,
    private _facebookTrackingEventService: FacebookTrackingEventService
  ) { }


  ngOnInit() {

    const body = document.getElementById('bodyElement');

    body.classList.remove('no-bg');

    this.teamMembers = [
      {
        'name': 'Louis Cleroux',
        'photo': 'louis-cleroux',
        'title': 'Chief Executive Officer',
        'linkedin': 'https://www.linkedin.com/in/lcleroux'
      },
      {
        'name': 'Phil Therien',
        'photo': 'phil-therien',
        'title': 'Chief Operating Officer',
        'linkedin': 'https://www.linkedin.com/in/philtherien'
      },
      {
        'name': 'Pier-Luc Lafleur',
        'photo': 'pl-lafleur',
        'title': 'Chief Technology Officer',
        'linkedin': 'https://www.linkedin.com/in/plucmtl'
      },
      {
        'name': 'Andre Dupont',
        'photo': 'andre-dupont',
        'title': 'VP Strategy',
        'linkedin': 'https://www.linkedin.com/in/andredupontmoguel'
      },
      {
        'name': 'Julien (Liang) Zhao',
        'photo': 'julien-zhao',
        'title': 'VP Investor Relations',
        'linkedin': 'https://www.linkedin.com/in/montrealzhaoliang/'
      },
      {
        'name': 'Kosta Kostic',
        'photo': 'kosta-kostic',
        'title': 'Legal Counsel',
        'linkedin': 'https://www.linkedin.com/in/kosta-kostic-273804'
      },
      {
        'name': 'Liliana (HuiYing) Sun',
        'photo': 'liliana-sun',
        'title': 'VP Audit and Compliance',
        'linkedin': 'https://www.linkedin.com/in/lillinahuiyingsun/'
      }
    ];

    this.advisors = [
      {
        'name': 'Louis Doyle',
        'title': 'Market Advisor',
        'photo': 'louis-doyle',
        'linkedin': 'https://www.linkedin.com/in/louis-doyle-2989b31'
      },
      {
        'name': 'Louis Turp',
        'title': 'Venture Advisor',
        'photo': 'louis-turp',
        'linkedin': 'https://www.linkedin.com/in/louis-turp-5899611'
      },
      {
        'name': 'Jack Luo',
        'title': 'Blockchain Advisor',
        'photo': 'jack-luo',
        'linkedin': 'https://www.linkedin.com/in/jackwluo'
      },
      {
        'name': 'Alexandre Cote',
        'title': 'Financial Markets Advisor',
        'photo': 'alex-cote',
        'linkedin': 'https://www.linkedin.com/in/alexandre-c%C3%B4t%C3%A9-0102951'
      },
      {
        'name': 'Charles Cao',
        'photo': 'charles-cao',
        'title': 'AI & Blockchain Advisor',
        'linkedin': 'https://www.linkedin.com/in/charles-cao-09a79526'
      },
      {
        'name': 'Kyle Kemper',
        'photo': 'kyle-kemper',
        'title': 'Wallet Advisor',
        'linkedin': 'https://www.linkedin.com/in/kylekemper'
      },
      {
        'name': 'Damien Michael Nichols',
        'photo': 'damien-michael-nichols',
        'title': 'Community Advisor',
        'linkedin': 'https://www.linkedin.com/in/damienmichaelnichols'
      },
      {
        'name': 'Lucky Ukwakwe',
        'photo': 'lucky-ukwakwe',
        'title': 'Strategic Advisor',
        'linkedin': 'https://www.linkedin.com/in/lucky-uwakwe-a66949b7/'
      }
    ];

    this.logos = [
      {
        'path': 'bitcoin-logo',
        'name': 'bitcoin'
      },
      {
        'path': 'eos-logo',
        'name': 'eos'
      },
      {
        'path': 'ethereum-logo',
        'name': 'ethereum'
      },
      {
        'path': 'litecoin-logo',
        'name': 'litecoin'
      },
      {
        'path': 'ripple-logo',
        'name': 'ripple'
      },
      {
        'path': 'stellar-logo',
        'name': 'stellar'
      },
      {
        'path': 'bitcoin-cash-logo',
        'name': 'bitcoin-cash'
      },
      {
        'path': 'nem-logo',
        'name': 'nem'
      },
      {
        'path': 'cardano-logo',
        'name': 'cardano'
      },
      {
        'path': 'neo-logo',
        'name': 'neo'
      }
    ];

    this.infos = [
      {
        'title': 'Timechain MetaWallet is the only wallet you’ll ever need',
        'text': 'The Timechain MetaWallet will store more than 1000 different cryptocurrencies, allowing users to create multiple same-chain wallets and categories to simplify asset management.',
        'img': 'payment'
      },
      {
        'title': 'Integrated centralized and decentralized exchanges',
        'text': 'Our decentralized exchange supports many different coins and eliminates custodianship risk. Our centralized exchange has a built-in cold storage for the top-20 most traded cryptos.',
        'img': 'arrows'
      },
      {
        'title': 'Sell and purchase your crypto directly from your wallet',
        'text': 'Timechain eliminates the need for external/online exchanges by allowing users to purchase or sell their cryptocurrencies directly from their digital wallet.',
        'img': 'database'
      },
      {
        'title': 'Manage your digital assets through a secure, audited and trusted platform',
        'text': 'Timechain has been developed according to today\'s security standards, and keeping tomorrow\'s concerns in check. We\'re continuously updating our code and infrastructure.',
        'img': 'lock'
      },
    ];
  }

  openDialog(name: string, category: string, width?: string, height?: string): void {
    this._facebookTrackingEventService.trackEvent(name, category);
    const dialogRef = this.dialog.open(DialogComponent, {
      width: width,
      height: height
    });
  }

  trackEvent(name: string, category: string): void {
    this._facebookTrackingEventService.trackEvent(name, category);
  }

}
