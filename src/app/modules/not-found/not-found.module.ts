import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './not-found.component';

const appRoutes: Routes = [
  {
    path: '',
    component: NotFoundComponent
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      appRoutes,
    ),
  ],
  declarations: [
    NotFoundComponent,

  ],
  exports: [
    NotFoundComponent,
    RouterModule
  ],
})
export class NotFoundModule { }



