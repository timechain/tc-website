import { Injectable } from '@angular/core';

declare const fbq: any;

@Injectable()
export class FacebookTrackingEventService {

    constructor() {
    }

    trackEvent(name: string, category: string): void {
        fbq('track', 'Lead', {
            content_name: name,
            content_category: category
        });
    }

}