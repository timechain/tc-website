import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public currentLocation: string;

  constructor(private router: Router,
              private location: Location) {
  }

  ngOnInit() {
    this.router.events.subscribe((val) => {
      this.currentLocation = this.location.path();
    });
  }

}
