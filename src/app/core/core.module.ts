import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { NavComponent } from './nav/nav.component';
import { DialogComponent } from './dialog/dialog.component';
import { MatInputModule, MatFormFieldModule, MatButtonModule, MatDialogModule, MatFormFieldControl, MatDialogClose } from '@angular/material';
import { FacebookTrackingEventService } from '../services/facebook-tracking-event-service';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDialogModule,
    FormsModule
  ],
  declarations: [
    DialogComponent,
    FooterComponent,
    NavComponent
  ],
  exports: [
    DialogComponent,
    FooterComponent,
    NavComponent
  ],
  entryComponents: [
    DialogComponent
  ],
  providers: [
    FacebookTrackingEventService
  ]
})
export class CoreModule { }



