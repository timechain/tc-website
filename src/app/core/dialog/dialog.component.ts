import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatInput, MatFormField, MatButton, MatDialogClose, MatDialogContent, MatDialogTitle, MatDialogActions } from '@angular/material';
import { FacebookTrackingEventService } from '../../services/facebook-tracking-event-service';


@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
  providers: [FacebookTrackingEventService]
})
export class DialogComponent {

  public errorMessage: string
  public isDisabled: boolean

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    public _facebookTrackingEventService: FacebookTrackingEventService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.errorMessage = ''
    this.isDisabled = true;
  }

  validateEmail(e): void {
    let email = e.target.value
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(email)) {
      console.log("valide")
      this.errorMessage = ''
      this.isDisabled = false;
    } else {
      this.errorMessage = 'Please enter a valid email.'
      this.isDisabled = true;
    }
  }

  onNoClick(name: string, category: string): void {
    this._facebookTrackingEventService.trackEvent(name, category)
    this.dialogRef.close();
  }
}
