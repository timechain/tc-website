import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';

import { faTelegramPlane } from '@fortawesome/fontawesome-free-brands';
import fontawesome from '@fortawesome/fontawesome';
import { FacebookTrackingEventService } from '../../services/facebook-tracking-event-service';

fontawesome.library.add(faTelegramPlane);

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
  providers: [FacebookTrackingEventService]
})

export class NavComponent implements OnInit {

  @ViewChild('navbarResponsive') navbar: ElementRef;
  public isOpen: boolean;
  public animal: string;

  constructor(
    private _facebookTrackingEventService: FacebookTrackingEventService
  ) { }

  ngOnInit() {
    this.isOpen = false;
  }

  openNav(): void {
    const body = document.getElementById('bodyElement');
    if (this.isOpen) {
      this.isOpen = false;
      body.classList.remove('isOpen');
    } else {
      this.isOpen = true;
      body.className = 'isOpen';
    }
  }

  trackEvent(name: string, category: string): void {
    this._facebookTrackingEventService.trackEvent(name, category);
  }
}
